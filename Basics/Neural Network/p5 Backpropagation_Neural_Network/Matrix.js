function sigmoid(val) {
    return 1 / (1 + Math.exp(-val));
}

function sigmoidDerivative(val) {
    return val * (1 - val);
}

function relu(val) {
    return max(0, val);
}

function reluDerivative(val) {
    if (val < 0)
        return 0;
    else
        return 1;
}

class Matrix {
    constructor(rows, columns) {
        this.rows = rows;
        this.columns = columns;
        this.data = [];

        for (let i = 0; i < this.rows; ++ i) {
            this.data[i] = [];
            for (let j = 0; j < this.columns; ++ j) {
                this.data[i][j] = 0;
            }
        }
    }

    randomize() {
        for (let i = 0; i < this.rows; ++ i) {
            for (let j = 0; j < this.columns; ++ j) {
                this.data[i][j] = 1 - 2 * Math.random();
            }
        }
    }

    add(n) {
        if (n instanceof Matrix && n.rows == this.rows && n.columns == this.columns) {
            for (let i = 0; i < this.rows; ++ i) {
                for (let j = 0; j < this.columns; ++ j) {
                    this.data[i][j] += n.data[i][j];
                }
            }
        }
        else if (typeof n == 'number') {
            for (let i = 0; i < this.rows; ++ i) {
                for (let j = 0; j < this.columns; ++ j) {
                    this.data[i][j] += n;
                }
            }
        }
    }

    static add(a, b) {
        if (a instanceof Matrix && b instanceof Matrix && a.rows == b.rows && a.columns == b.columns) {
            let result = new Matrix(a.rows, a.columns);
            for (let i = 0; i < a.rows; ++ i) {
                for (let j = 0; j < a.columns; ++ j) {
                    result.data[i][j] = a.data[i][j] + b.data[i][j];
                }
            }
            return result;
        }
    }

    substract(n) {
        if (n instanceof Matrix && this.rows == n.rows && this.columns == n.columns) {
            for (let i = 0; i < this.rows; ++ i) {
                for (let j = 0; j < this.columns; ++ j) {
                    this.data[i][j] -= n.data[i][j];
                }
            }
        }
        else if (typeof n == 'number') {
            for (let i = 0; i < this.rows; ++ i) {
                for (let j = 0; j < this.columns; ++ j) {
                    this.data[i][j] -= n;
                }
            }
        }
    }

    static substract(a, b) {
        if (a instanceof Matrix && b instanceof Matrix && a.rows == b.rows && a.columns == b.columns) {
            let result = new Matrix(a.rows, a.columns);
            for (let i = 0; i < result.rows; ++ i) {
                for (let j = 0; j < result.columns; ++ j) {
                    result.data[i][j] = a.data[i][j] - b.data[i][j];
                }
            }
            return result;
        }
    }

    multiply(n) {
        if (typeof n == 'number') {
            for (let i = 0; i < this.rows; ++ i) {
                for (let j = 0; j < this.columns; ++ j) {
                    this.data[i][j] *= n;
                }
            }
        }
    }

    static multiply(a, b) {
        if (a instanceof Matrix && b instanceof Matrix && a.columns == b.rows) {
            let result = new Matrix(a.rows, b.columns);
            for (let i = 0; i < result.rows; ++ i) {
                for (let j = 0; j < result.columns; ++ j) {
                    for (let k = 0; k < a.columns; ++ k) {
                        result.data[i][j] += a.data[i][k] * b.data[k][j];
                    }
                }
            }
            return result;
        }
    }

    elementwiseMultiply(n) {
        if (n instanceof Matrix && this.rows == n.rows && this.columns == n.columns) {
            for (let i = 0; i < this.rows; ++ i) {
                for (let j = 0; j < this.columns; ++ j) {
                    this.data[i][j] = this.data[i][j] * n.data[i][j];
                }
            }
        }
    }

    static elementwiseMultiply(a, b) {
        if (a instanceof Matrix && b instanceof Matrix && a.rows == b.rows && a.columns == b.columns) {
            let result = new Matrix(a.rows, a.columns);
            for (let i = 0; i < result.rows; ++ i) {
                for (let j = 0; j < result.columns; ++ j) {
                    result.data[i][j] = a.data[i][j] * b.data[i][j];
                }
            }
            return result;
        }
    }

    static transpose(n) {
        if (n instanceof Matrix) {
            let result = new Matrix(n.columns, n.rows);
            for (let i = 0; i < result.rows; ++ i)
                for (let j = 0; j < result.columns; ++ j)
                    result.data[i][j] = n.data[j][i];
            return result;
        }
    }

    applyFunction(func) {
        for (let i = 0; i < this.rows; ++ i) {
            for (let j = 0; j < this.columns; ++ j) {
                this.data[i][j] = func(this.data[i][j]);
            }
        }
    }

    copy() {
        let result = new Matrix(this.rows, this.columns);
        for (let i = 0; i < this.rows; ++ i) {
            for (let j = 0; j < this.columns; ++ j) {
                result.data[i][j] = this.data[i][j];
            }
        }
        return result;
    }
}