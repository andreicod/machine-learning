let nn = new NeuralNetwork(2, 3, 2, 1);

let trainingData = [
    [0, 0],
    [1, 1],
    [0, 1],
    [1, 0]
];
let labels = [
    [0],
    [0],
    [1],
    [1]
];

function setup() {
    createCanvas(400, 400);
    nn.randomizeNeuralNetwork();

    lrSlider = createSlider(0.01, 0.5, 0.01, 0.01);
}

let speed = 1000;
let l = 10;

function draw() {
    nn.setLearningRate(lrSlider.value());

    for (let i = 0; i < speed; ++ i) {
        let index = floor(random(4));
        nn.backPropagate(trainingData[index], labels[index]);
    }
    for (let i = 0; i < width / l; ++ i) {
        for (let j = 0; j < height / l; ++ j) {
            let inputData = [i / (width / l), j / (height / l)];
            let guess = nn.feedForward(inputData)[0];
            noStroke();
            fill(guess * 255);
            square(i * l, j * l, l);
        }
    }
}