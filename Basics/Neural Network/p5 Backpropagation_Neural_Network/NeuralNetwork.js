class NeuralNetwork {
    constructor(inputN, hiddenCnt, hiddenN, outputN) {
        this.inputN = inputN;
        this.hiddenCnt = hiddenCnt;
        this.hiddenN = hiddenN;
        this.outputN = outputN;

        this.input = new Matrix(inputN, 1);
        this.hidden = [];
        for (let i = 0; i < hiddenCnt; ++ i)
            this.hidden[i] = new Matrix(hiddenN, 1);
        this.output = new Matrix(outputN, 1);

        this.IHWeights = new Matrix(hiddenN, inputN);
        this.IHBias = new Matrix(hiddenN, 1);
        this.HHWeights = [];
        for (let i = 0; i < hiddenCnt - 1; ++ i)
            this.HHWeights[i] = new Matrix(hiddenN, hiddenN);
        this.HHBias = [];
        for (let i = 0; i < hiddenCnt - 1; ++ i)
            this.HHBias[i] = new Matrix(hiddenN, 1);
        this.HOWeights = new Matrix(outputN, hiddenN);
        this.HOBias = new Matrix(outputN, 1);

        this.learningRate = 0.01;
    }

    setLearningRate(val) {
        this.learningRate = val;
    }

    randomizeNeuralNetwork() {
        this.IHWeights.randomize();
        for (let i = 0; i < this.HHWeights.length; ++ i)
            this.HHWeights[i].randomize();
        this.HOWeights.randomize();

        this.IHBias.randomize();
        for (let i = 0; i < this.HHBias.length; ++ i)
            this.HHBias[i].randomize();
        this.HOBias.randomize();
    }

    feedForward(inputData) {
        if (typeof inputData == 'object') {
            for (let i = 0; i < this.inputN; ++ i)
                this.input.data[i][0] = inputData[i];

            this.hidden[0] = Matrix.multiply(this.IHWeights, this.input);
            this.hidden[0].add(this.IHBias);
            this.hidden[0].applyFunction(sigmoid);
            

            for (let k = 1; k < this.hiddenCnt; ++ k) {
                this.hidden[k] = Matrix.multiply(this.HHWeights[k - 1], this.hidden[k - 1]);
                this.hidden[k].add(this.HHBias[k - 1]);
                this.hidden[k].applyFunction(sigmoid);
            }

            this.output = Matrix.multiply(this.HOWeights, this.hidden[this.hiddenCnt - 1]);
            this.output.add(this.HOBias);
            this.output.applyFunction(sigmoid);

            let results = [];
            for (let i = 0; i < this.outputN; ++ i)
                results[i] = this.output.data[i][0];
            return results;
        }
    }

    backPropagate(inputData, targetData) {
        if (typeof inputData == 'object' && typeof targetData == 'object') {
            this.feedForward(inputData);

            let target = new Matrix(this.outputN, 1);
            for (let i = 0; i < this.outputN; ++ i)
                target.data[i][0] = targetData[i];
            
            let error = Matrix.substract(target, this.output);
            this.output.applyFunction(sigmoidDerivative);
            error.elementwiseMultiply(this.output);
            error.multiply(this.learningRate);
            let weightsGradient = Matrix.multiply(error, Matrix.transpose(this.hidden[this.hiddenCnt - 1]));
            let biasGradient = error.copy();
            let layerGradient = Matrix.multiply(Matrix.transpose(this.HOWeights), error);

            this.HOWeights.add(weightsGradient);
            this.HOBias.add(biasGradient);

            for (let i = this.hiddenCnt - 1; i > 0; -- i) {
                error = layerGradient.copy();
                this.hidden[i].applyFunction(sigmoidDerivative);
                error.elementwiseMultiply(this.hidden[i]);
                error.multiply(this.learningRate);
                weightsGradient = Matrix.multiply(error, Matrix.transpose(this.hidden[i - 1]));
                biasGradient = error.copy();
                layerGradient = Matrix.multiply(Matrix.transpose(this.HHWeights[i - 1]), error);

                this.HHWeights[i - 1].add(weightsGradient);
                this.HHBias[i - 1].add(biasGradient);
            }

            error = layerGradient.copy();
            this.hidden[0].applyFunction(sigmoidDerivative);
            error.elementwiseMultiply(this.hidden[0]);
            error.multiply(this.learningRate);
            weightsGradient = Matrix.multiply(error, Matrix.transpose(this.input));
            biasGradient = error.copy();
            layerGradient = Matrix.multiply(Matrix.transpose(this.IHWeights), error);

            this.IHWeights.add(weightsGradient);
            this.IHBias.add(biasGradient);
        }
    }

}