void randomize(Matrix a) {
	for (int i = 0; i < a.rows; ++ i)
		for (int j = 0; j < a.columns; ++ j)
			a.data[i][j] = random(-1, 1);
}

static float sigmoid(float nr) {
	return 1.0 / (1.0 + exp(-nr));
}

static float sigmoidDerivative(float nr) {
	return nr * (1 - nr);
}

static class Matrix {
	int rows, columns;
	float[][] data;

	Matrix(int _rows, int _columns) {
		rows = _rows;
		columns = _columns;
		data = new float[rows][columns];
	}

	void printData() {
		for (int i = 0; i < data.length; ++ i) {
			for (int j = 0; j < data[i].length; ++ j) {
				print(data[i][j]);
				print(" ");
			}
			println();
		}
	}

	static Matrix add(Matrix a, Matrix b) {
		Matrix c = new Matrix(a.rows, a.columns);
		if (a.rows == b.rows && a.columns == b.columns)
			for (int i = 0; i < a.rows; ++ i)
				for (int j = 0; j < a.columns; ++ j)
					c.data[i][j] = a.data[i][j] + b.data[i][j];
		return c;
	}

	void add(Matrix a) {
		if (a.rows == rows && a.columns == columns)
			for (int i = 0; i < data.length; ++ i)
				for (int j = 0; j < data[i].length; ++ j)
					data[i][j] += a.data[i][j];
	}

	static Matrix substract(Matrix a, Matrix b) {
		Matrix c = new Matrix(a.rows, a.columns);
		if (a.rows == b.rows && a.columns == b.columns)
			for (int i = 0; i < a.data.length; ++ i)
				for (int j = 0; j < a.data[i].length; ++ j)
					c.data[i][j] = a.data[i][j] - b.data[i][j];
		return c;
	}

	void substract(Matrix a) {
		if (a.rows == rows && a.columns == columns)
			for (int i = 0; i < data.length; ++ i)
				for (int j = 0; j < data[i].length; ++ j)
					data[i][j] -= a.data[i][j];
	}

	static Matrix multiply(Matrix a, Matrix b) {
		Matrix c = new Matrix(a.rows, b.columns);
		if (a.columns == b.rows)
			for (int i = 0; i < c.rows; ++ i)
				for (int j = 0; j < c.columns; ++ j)
					for (int k = 0; k < a.columns; ++ k)
						c.data[i][j] += a.data[i][k] * b.data[k][j];
		return c;
	}

	static Matrix multiply(Matrix a, float b) {
		Matrix c = new Matrix(a.rows, a.columns);
		for (int i = 0; i < c.rows; ++ i)
			for (int j = 0; j < c.columns; ++ j)
				c.data[i][j] = a.data[i][j] * b;
		return c;
	}

	static Matrix elementwiseMultiply(Matrix a, Matrix b) {
		Matrix c = new Matrix(a.rows, a.columns);
		if (a.rows == b.rows && a.columns == b.columns)
			for (int i = 0; i < c.rows; ++ i)
				for (int j = 0; j < c.columns; ++ j)
					c.data[i][j] = a.data[i][j] * b.data[i][j];
		return c;
	}

	void multiply(float a) {
		for (int i = 0; i < rows; ++ i)
			for (int j = 0; j < columns; ++ j)
				data[i][j] *= a;
	}

	static Matrix transpose(Matrix a) {
		Matrix b = new Matrix(a.columns, a.rows);
		for (int i = 0; i < a.rows; ++ i)
			for (int j = 0; j < a.columns; ++ j)
				b.data[j][i] = a.data[i][j];
		return b;
	}

	void applyActivationFunction() {
		for (int i = 0; i < rows; ++ i)
			for (int j = 0; j < columns; ++ j)
				data[i][j] = sigmoid(data[i][j]);
	}

	static Matrix applyDerivative(Matrix a) {
		Matrix c = new Matrix(a.rows, a.columns);
		for (int i = 0; i < c.rows; ++ i)
			for (int j = 0; j < c.columns; ++ j)
				c.data[i][j] = sigmoidDerivative(a.data[i][j]);
		return c;
	}
}