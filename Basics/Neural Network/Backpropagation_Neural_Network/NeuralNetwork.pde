class NeuralNetwork {
	int inputN, hiddenN, outputN;
	Matrix input, hidden, output;
	Matrix weightsIH, biasH, weightsHO, biasO;
	float learningRate = 0.1;

	NeuralNetwork(int _inputN, int _hiddenN, int _outputN) {
		inputN = _inputN;
		outputN = _outputN;
		hiddenN = _hiddenN;

		input = new Matrix(inputN, 1);
		hidden = new Matrix(hiddenN, 1);
		output = new Matrix(outputN, 1);

		weightsIH = new Matrix(hiddenN, inputN);
		biasH = new Matrix(hiddenN, 1);
		weightsHO = new Matrix(outputN, hiddenN);
		biasO = new Matrix(outputN, 1);
		randomize(weightsIH); randomize(biasH); randomize(weightsHO); randomize(biasO);
	}

	float[] feedForward(float[] inputArray) {
		for (int i = 0; i < inputArray.length; ++ i)
			input.data[i][0] = inputArray[i];

		hidden = Matrix.multiply(weightsIH, input);
		hidden.add(biasH);
		hidden.applyActivationFunction(); 
		output = Matrix.multiply(weightsHO, hidden);
		output.add(biasO);
		output.applyActivationFunction();

		float[] outputArray = new float[outputN];
		for (int i = 0; i < outputN; ++ i)
			outputArray[i] = output.data[i][0];

		return outputArray;
	}

	void backPropagate(float[] inputArray, float[] labels) {
		feedForward(inputArray);

		Matrix target = new Matrix(outputN, 1);
		for (int i = 0; i < outputN; ++ i)
			target.data[i][0] = labels[i];

		Matrix output_error = Matrix.substract(target, output);
		Matrix outputD = Matrix.applyDerivative(output);
		Matrix output_product = Matrix.elementwiseMultiply(output_error, outputD);
		Matrix hiddenT = Matrix.transpose(hidden);
		Matrix gradientWeightsHO = Matrix.multiply(Matrix.multiply(output_product, hiddenT), learningRate);
		weightsHO.add(gradientWeightsHO);
		Matrix gradientBiasO = Matrix.multiply(output_product, learningRate);
		biasO.add(gradientBiasO);

		Matrix weightsHOT = Matrix.transpose(weightsHO);
		Matrix hidden_error = Matrix.multiply(Matrix.multiply(weightsHOT, output_product), learningRate);
		Matrix hiddenD = Matrix.applyDerivative(hidden);
		Matrix hidden_product = Matrix.elementwiseMultiply(hidden_error, hiddenD);
		Matrix inputT = Matrix.transpose(input);
		Matrix gradientWeightsIH = Matrix.multiply(Matrix.multiply(hidden_product, inputT), learningRate);
		weightsIH.add(gradientWeightsIH);
		Matrix gradientBiasH = Matrix.multiply(hidden_product, learningRate);
		biasH.add(gradientBiasH);
	}

	void view(float scale) {
		float r = scale;
		float l = 2 * r;
		float h = max(inputN, hiddenN, outputN) * l;
		float w = 3 * l;

		float multiplier = 100;

		// input -> hidden weights
		float padX1 = l / 2;
		float padX2 = padX1 + l;
		float padY1 = (h - (l * inputN)) / 2 + (l / 2);
		float padY2 = (h - (l * hiddenN)) / 2 + (l / 2);
		for (int i = 0; i < hiddenN; ++ i) {
			for (int j = 0; j < inputN; ++ j) {
				strokeWeight(4);
				stroke(255);
				line(padX1, padY1 + j * l, padX2, padY2 + i * l);
				if (weightsIH.data[i][j] > 0)
					stroke(0, 255, 0, multiplier * weightsIH.data[i][j]);
				else
					stroke(255, 0, 0, -multiplier * weightsIH.data[i][j]);
				line(padX1, padY1 + j * l, padX2, padY2 + i * l);
			}
		}

		// hidden -> output weights
		padX1 = 3 * l / 2;
		padX2 = padX1 + l;
		padY1 = (h - (l * hiddenN)) / 2 + (l / 2);
		padY2 = (h - (l * outputN)) / 2 + (l / 2);
		for (int i = 0; i < outputN; ++ i) {
			for (int j = 0; j < hiddenN; ++ j) {
				strokeWeight(4);
				stroke(255);
				line(padX1, padY1 + j * l, padX2, padY2 + i * l);
				if (weightsHO.data[i][j] > 0)
					stroke(0, 255, 0, multiplier * weightsHO.data[i][j]);
				else
					stroke(255, 0, 0, -multiplier * weightsHO.data[i][j]);
				line(padX1, padY1 + j * l, padX2, padY2 + i * l);
			}
		}

		// input layer
		float padY = (h - (l * inputN)) / 2 + (l / 2);
		float padX = l / 2;
		for (int i = 0; i < inputN; ++ i) {
			noStroke();
			fill(255);
			circle(padX, padY + i * l, r);
		}


		// hidden layer
		padY = (h - (l * hiddenN)) / 2 + (l / 2);
		padX = 3 * l / 2;
		for (int i = 0; i < hiddenN; ++ i) {
			noStroke();
			fill(255);
			circle(padX, padY + i * l, r);
			noStroke();
			if (biasH.data[i][0] > 0.0)
				fill(0, 255, 0, multiplier * biasH.data[i][0]);
			else
				fill(255, 0, 0, -multiplier * biasH.data[i][0]);
			circle(padX, padY + i * l, r);
		}

		// output layer
		padY = (h - (l * outputN)) / 2 + (l / 2);
		padX = 5 * l / 2;
		for (int i = 0; i < outputN; ++ i) {
			noStroke();
			fill(255);
			circle(padX, padY + i * l, r);
			noStroke();
			if (biasO.data[i][0] > 0.0)
				fill(0, 255, 0, multiplier * biasO.data[i][0]);
			else
				fill(255, 0, 0, -multiplier * biasO.data[i][0]);
			circle(padX, padY + i * l, r);
		}

	}

}