NeuralNetwork nn = new NeuralNetwork(2, 2, 1);
float[][] inputs = {{1, 0}, {0, 1}, {1, 1}, {0, 0}};
float[][] labels = {{1}, {1}, {0}, {0}};

void setup() {
	size(800, 800);
}

float l = 10;
int speed = 1000;

void draw() {
	background(255);

	for (int i = 0; i < speed; ++ i) {
		int trainingIndex = floor(random(inputs.length));
		nn.backPropagate(inputs[trainingIndex], labels[trainingIndex]);
	}

	for (float i = 0; i * l < height; ++ i) {
		for (float j = 0; j * l < width; ++ j) {
			noStroke();
			float[] input = {i / (height / l), j / (width / l)};
			fill(nn.feedForward(input)[0] * 255);
			square(j * l, i * l, l);
		}
	}

	nn.view(50);

	delay(10);
}