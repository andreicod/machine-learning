import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.ArrayList; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Ordinary_least_squares extends PApplet {



class Point {
	float x, y;
	Point() {
		x = mouseX;
		y = height - mouseY;
	}

	public void show() {
		fill(0);
		ellipse(x, height - y, 16, 16);
	}
}

class Line {
	float b1, b0;

	Line() {

	}

	public void update() {
		if (points.size() > 1) {
			float avgX = 0;
			float avgY = 0;
			for (Point point : points) {
				avgX += point.x;
				avgY += point.y;
			}
			avgX /= points.size();
			avgY /= points.size();

			float num = 0, den = 0;
			for (Point point : points) {
				num += (point.x - avgX) * (point.y - avgY);
				den += (point.x - avgX) * (point.x - avgX);
			}

			b1 = num / den;
			b0 = avgY - b1 * avgX;
		}
	}

	public void show() {
		if (points.size() > 1) {
			stroke(0);
			strokeWeight(3);
			line(0, height - b0, width, height - (b1 * width + b0));
		}
	}
}

ArrayList<Point> points = new ArrayList<Point>();
Line line = new Line();

public void setup() {
	
}

public void draw() {
	background(255, 1000);
	for (Point point : points)
		point.show();
	line.show();
}

public void mouseClicked() {
	points.add(new Point());
	line.update();
}
  public void settings() { 	size(800, 800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Ordinary_least_squares" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
