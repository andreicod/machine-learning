import java.util.ArrayList;

class Point {
	float x, y;
	Point() {
		x = mouseX;
		y = height - mouseY;
	}

	void show() {
		fill(0);
		ellipse(x, height - y, 16, 16);
	}
}

class Line {
	float b1, b0;

	Line() {

	}

	void update() {
		if (points.size() > 1) {
			float avgX = 0;
			float avgY = 0;
			for (Point point : points) {
				avgX += point.x;
				avgY += point.y;
			}
			avgX /= points.size();
			avgY /= points.size();

			float num = 0, den = 0;
			for (Point point : points) {
				num += (point.x - avgX) * (point.y - avgY);
				den += (point.x - avgX) * (point.x - avgX);
			}

			b1 = num / den;
			b0 = avgY - b1 * avgX;
		}
	}

	void show() {
		if (points.size() > 1) {
			stroke(0);
			strokeWeight(3);
			line(0, height - b0, width, height - (b1 * width + b0));
		}
	}
}

ArrayList<Point> points = new ArrayList<Point>();
Line line = new Line();

void setup() {
	size(800, 800);
}

void draw() {
	background(255, 1000);
	for (Point point : points)
		point.show();
	line.show();
}

void mouseClicked() {
	points.add(new Point());
	line.update();
}