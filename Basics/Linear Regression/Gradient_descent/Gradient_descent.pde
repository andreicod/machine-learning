import java.util.ArrayList;

class Point {
	float x, y;
	Point() {
		x = map(mouseX, 0, width, 0, 1);
		y = map(mouseY, 0, height, 1, 0);
	}

	void show() {
		fill(0);
		float showX = map(x, 0, 1, 0, width);
		float showY = map(y, 0, 1, height, 0);
		ellipse(showX, showY, 16, 16);
	}
}

ArrayList<Point> points = new ArrayList<Point>();

class Line {
	float b1, b0;
	float learningRate = 0.05;
	Line() {

	}

	void update() {
		if (points.size() > 1) {
			for (Point point : points) {
				float x = point.x;
				float y = point.y;
				float guess = b1 * x + b0;
				float error = y - guess;
				b1 = b1 + x * error * learningRate;
				b0 = b0 + error * learningRate;
			}
		}
	}

	void show() {
		stroke(0);
		strokeWeight(3);
		float x1 = 0;
		float y1 = b1 * x1 + b0;
		float x2 = 1;
		float y2 = b1 * x2 + b0;
		x1 = map(x1, 0, 1, 0, width);
		y1 = map(y1, 0, 1, height, 0);
		x2 = map(x2, 0, 1, 0, width);
		y2 = map(y2, 0, 1, height, 0);

		line(x1, y1, x2, y2);
	}
}

Line line = new Line();

void setup() {
	size(800, 800);
}

void draw() {
	background(255, 1000);
	for (Point point : points)
		point.show();
	line.update();
	line.show();
}

void mouseClicked() {
	points.add(new Point());
}