int sign(float nr) {
	if (nr >= 0) return 1;
	return -1;
}

class Perceptron {
	float weights[] = new float[2];
	float bias;
	float learningRate = 0.005;

	Perceptron() {
		for (int i = 0; i < weights.length; ++ i) {
			weights[i] = random(-1, 1);
		}
		bias = random(-1, 1);
	}

	int guess(float[] inputs) {
		float sum = 0;
		for (int i = 0; i < weights.length; ++ i) {
			sum += inputs[i] * weights[i];
		}
		int output = sign(sum + bias);
		return output;
	}

	void train(float[] inputs, int label) {
		int guess = guess(inputs);
		int target = label;
		float error = target - guess;
		for (int i = 0; i < weights.length; ++ i)
			weights[i] += error * inputs[i] * learningRate;
		bias += error * learningRate;
	}

	float guessY(float x) {
		float m = weights[0] / weights[1];
		float b = bias / weights[1];
		return -m * x - b;
	}
}