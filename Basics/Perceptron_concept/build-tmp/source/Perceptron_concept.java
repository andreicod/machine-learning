import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Perceptron_concept extends PApplet {

Point[] points = new Point[200];
Perceptron brain = new Perceptron();

public void setup() {
	

	for (int i = 0; i < points.length; ++ i) {
		points[i] = new Point();
	}
}

int trainingIndex = 0;

public void draw() {
	background(255);

	stroke(0);
	strokeWeight(2);
	// line(0, height, width, 0);

	Point p1 = new Point(-1, f(-1));
	Point p2 = new Point(1, f(1));
	line(p1.pixelX(), p1.pixelY(), p2.pixelX(), p2.pixelY());

	Point p3 = new Point(-1, brain.guessY(-1));
	Point p4 = new Point(1, brain.guessY(1));
	line(p3.pixelX(), p3.pixelY(), p4.pixelX(), p4.pixelY());

	for (Point p : points) {
		p.show();

		float[] inputs = {p.x, p.y};
		int guess = brain.guess(inputs);
		int target = p.label;

		if (guess == target) fill(0, 255, 0);
		else fill(255, 0, 0);

		noStroke();
		ellipse(p.pixelX(), p.pixelY(), 16, 16);
	}
	
	fill(0);
	ellipse(points[trainingIndex].pixelX(), points[trainingIndex].pixelY(), 20, 20);

	float[] inputs = {points[trainingIndex].x, points[trainingIndex].y};
	brain.train(inputs, points[trainingIndex].label);
	++ trainingIndex;
	trainingIndex %= points.length;
}

public void mouseClicked() {

}
public int sign(float nr) {
	if (nr >= 0) return 1;
	return -1;
}

class Perceptron {
	float weights[] = new float[2];
	float bias;
	float learningRate = 0.005f;

	Perceptron() {
		for (int i = 0; i < weights.length; ++ i) {
			weights[i] = random(-1, 1);
		}
		bias = random(-1, 1);
	}

	public int guess(float[] inputs) {
		float sum = 0;
		for (int i = 0; i < weights.length; ++ i) {
			sum += inputs[i] * weights[i];
		}
		int output = sign(sum + bias);
		return output;
	}

	public void train(float[] inputs, int label) {
		int guess = guess(inputs);
		int target = label;
		float error = target - guess;
		for (int i = 0; i < weights.length; ++ i)
			weights[i] += error * inputs[i] * learningRate;
		bias += error * learningRate;
	}

	public float guessY(float x) {
		float m = weights[0] / weights[1];
		float b = bias / weights[1];
		return -m * x - b;
	}
}
public float f(float x) {
	// y = mx + b
	return -0.89f * x + 0.5f;
}


class Point {
	float x;
	float y;
	int label;

	Point(float _x, float _y) {
		x = _x;
		y = _y;
	}

	Point() {
		x = random(-1, 1);
		y = random(-1, 1);

		if (y > f(x)) label = 1;
		else label = -1;
	}

	public float pixelX() {
		return map(x, -1, 1, 0, width);
	}

	public float pixelY() {
		return map(y, -1, 1, height, 0);
	}

	public void show() {
		stroke(0);
		if (label == 1) fill(255);
		else fill(0);
		float px = pixelX();
		float py = pixelY();
		ellipse(px, py, 8, 8);
	}
}
  public void settings() { 	size(800, 800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Perceptron_concept" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
