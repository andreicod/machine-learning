Point[] points = new Point[200];
Perceptron brain = new Perceptron();

void setup() {
	size(800, 800);

	for (int i = 0; i < points.length; ++ i) {
		points[i] = new Point();
	}
}

int trainingIndex = 0;

void draw() {
	background(255);

	stroke(0);
	strokeWeight(2);
	// line(0, height, width, 0);

	Point p1 = new Point(-1, f(-1));
	Point p2 = new Point(1, f(1));
	line(p1.pixelX(), p1.pixelY(), p2.pixelX(), p2.pixelY());

	Point p3 = new Point(-1, brain.guessY(-1));
	Point p4 = new Point(1, brain.guessY(1));
	line(p3.pixelX(), p3.pixelY(), p4.pixelX(), p4.pixelY());

	for (Point p : points) {
		p.show();

		float[] inputs = {p.x, p.y};
		int guess = brain.guess(inputs);
		int target = p.label;

		if (guess == target) fill(0, 255, 0);
		else fill(255, 0, 0);

		noStroke();
		ellipse(p.pixelX(), p.pixelY(), 16, 16);
	}
	
	fill(0);
	ellipse(points[trainingIndex].pixelX(), points[trainingIndex].pixelY(), 20, 20);

	float[] inputs = {points[trainingIndex].x, points[trainingIndex].y};
	brain.train(inputs, points[trainingIndex].label);
	++ trainingIndex;
	trainingIndex %= points.length;
}

void mouseClicked() {

}
