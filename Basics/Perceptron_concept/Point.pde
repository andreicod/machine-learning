float f(float x) {
	// y = mx + b
	return -0.89 * x + 0.5;
}


class Point {
	float x;
	float y;
	int label;

	Point(float _x, float _y) {
		x = _x;
		y = _y;
	}

	Point() {
		x = random(-1, 1);
		y = random(-1, 1);

		if (y > f(x)) label = 1;
		else label = -1;
	}

	float pixelX() {
		return map(x, -1, 1, 0, width);
	}

	float pixelY() {
		return map(y, -1, 1, height, 0);
	}

	void show() {
		stroke(0);
		if (label == 1) fill(255);
		else fill(0);
		float px = pixelX();
		float py = pixelY();
		ellipse(px, py, 8, 8);
	}
}